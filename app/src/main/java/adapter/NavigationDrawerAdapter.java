package adapter;

/**
 * Created by phong on 7/15/2015.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phong.pushnofication.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.NavDrawerItem;


/**
 *
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private final ArrayList<Integer> selected = new ArrayList<>();
    private int pos;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();


    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

//    public void delete(int position) {
//        data.remove(position);
//        notifyItemRemoved(position);
//    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate( R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());

        holder.itemView.setSelected(selectedItems.get(position, false));


        // selecting items




}



    @Override
    public int getItemCount() {
        return data.size();
    }
//    public int getView(int position)
//    {
//
//    }





    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener   {
        TextView title;
        RelativeLayout background;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            //background = (RelativeLayout)itemView.findViewById(R.id.row);
        }


        @Override
        public void onClick(View view) {
            Toast.makeText(context,"The Item Clicked is: "+getPosition(), Toast.LENGTH_SHORT).show();
            Log.e("click", "ok");
            // Save the selected positions to the SparseBooleanArray
            if (selectedItems.get(getPosition(), false)) {
                pos = getPosition();
                selectedItems.delete(pos);
                view.setSelected(false);


            }
            else {
                selectedItems.put(getPosition(), true);
                view.setSelected(true);
            }

        }
    }
}
