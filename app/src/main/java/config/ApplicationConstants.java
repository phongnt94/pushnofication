package config;

/**
 * Created by chien.phi on 7/17/2015.
 */
public class ApplicationConstants {
    // Php Application URL to store Reg ID created
     public static final String APP_SERVER_URL = "http://192.168.1.135/gcm/gcm.php?shareRegId=true";

    // Google Project Number
    public  static final String GOOGLE_PROJ_ID = "818634114084";
    // Message Key
    public static final String MSG_KEY = "m";
}
